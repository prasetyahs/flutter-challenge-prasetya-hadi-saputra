import 'package:flutter/material.dart';

class CompHeader extends StatelessWidget {
  const CompHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      height: 150,
      decoration: const BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/header-login.png"))),
    );
  }
}
