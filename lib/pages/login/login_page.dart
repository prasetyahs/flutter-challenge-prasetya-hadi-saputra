import 'package:flutter/material.dart';
import 'package:flutter_challange/component/comp_button.dart';
import 'package:flutter_challange/component/input_field.dart';
import 'package:flutter_challange/pages/login/component/comp_header.dart';
import 'package:flutter_challange/component/comp_logo.dart';
import 'package:flutter_challange/component/comp_scaffold.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  String userID = "", password = "";
  AlertDialog popUp(context, color, message, title) {
    return AlertDialog(
      title: Text(
        title,
        style: TextStyle(color: color),
      ),
      content: Text(message),
      actions: [
        TextButton(
            onPressed: () => Navigator.pop(context), child: const Text("OK")),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return CompScaffold(
        body: Stack(
      children: [
        const Align(
          alignment: Alignment.topLeft,
          child: CompHeader(),
        ),
        Positioned(
          bottom: MediaQuery.of(context).size.height - 130,
          left: MediaQuery.of(context).size.width - 245,
          child: const CompLogo(
            height: 70.0,
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      "Login",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    )),
                const SizedBox(
                  height: 10,
                ),
                Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      "Please sign in to continue",
                      style:
                          TextStyle(fontSize: 13, ),
                    )),
                const SizedBox(
                  height: 20,
                ),
                InputField(
                  obsecureText: false,
                  inputAction: TextInputAction.next,
                  onChange: (v) {
                    setState(() {
                      userID = v;
                    });
                  },
                  hint: "User ID",
                ),
                const SizedBox(
                  height: 30,
                ),
                InputField(
                  obsecureText: true,
                  inputAction: TextInputAction.done,
                  onChange: (v) {
                    setState(() {
                      password = v;
                    });
                  },
                  hint: "Password",
                ),
                const SizedBox(
                  height: 30,
                ),
                CompButton(
                  btnText: "LOGIN",
                  callBack: () {
                    if (userID.isEmpty || password.isEmpty) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return popUp(context, Colors.red,
                              "User ID atau Password kosong", "Kesalahan");
                        },
                      );
                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return popUp(context, Colors.green,
                                "Login Berhasil", "Berhasil");
                          });
                    }
                  },
                )
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30),
            child: RichText(
              text: TextSpan(
                  text: "Don't have an account? ",
                  style: TextStyle(color: Colors.grey.shade500),
                  children: const [
                    TextSpan(
                        text: "Sign Up",
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold))
                  ]),
            ),
          ),
        )
      ],
    ));
  }
}
