import 'package:flutter/material.dart';
import 'package:flutter_challange/pages/splash/component/comp_footer.dart';
import 'package:flutter_challange/pages/splash/component/comp_header.dart';
import 'package:flutter_challange/component/comp_logo.dart';
import 'package:flutter_challange/component/comp_scaffold.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      await Future.delayed(const Duration(seconds: 2))
          .whenComplete(() => Navigator.popAndPushNamed(context, "/login"));
    });
  }

  @override
  Widget build(BuildContext context) {
    return CompScaffold(
      body: Stack(
        children: const [
          Align(alignment: Alignment.topCenter, child: CompHeader()),
          Align(
            alignment: Alignment.center,
            child: CompLogo(),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: CompFooter(),
          )
        ],
      ),
    );
  }
}
