import 'package:flutter/widgets.dart';

class CompFooter extends StatelessWidget {
  const CompFooter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: const BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/footer-splash.png"))),
    );
  }
}
