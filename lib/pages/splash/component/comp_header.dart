import 'package:flutter/material.dart';

class CompHeader extends StatelessWidget {
  const CompHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: const BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/header-splash.png"))),
    );
  }
}
