// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/widgets.dart';

class CompLogo extends StatelessWidget {
  final width, height;
  const CompLogo({Key? key, this.width, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "assets/images/logo.png",
      width: width,
      height: height,
    );
  }
}
