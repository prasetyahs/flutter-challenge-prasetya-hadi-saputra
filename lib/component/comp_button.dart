import 'package:flutter/material.dart';

class CompButton extends StatelessWidget {
  final String btnText;
  // ignore: prefer_typing_uninitialized_variables
  final callBack;
  const CompButton({Key? key, required this.btnText, this.callBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: callBack,
      child: Container(
          decoration: BoxDecoration(
              color: const Color.fromRGBO(100, 60, 164, 1),
              borderRadius: BorderRadius.circular(50)),
          padding: const EdgeInsets.symmetric(horizontal: 50,vertical: 15),
          child: Text(
            btnText,
            style: const TextStyle(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
          )),
    );
  }
}
