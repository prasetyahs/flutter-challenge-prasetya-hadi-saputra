// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  final String hint;
  // ignore: prefer_typing_uninitialized_variables
  final onChange;
  final inputAction,obsecureText;
  const InputField({Key? key, required this.hint, this.onChange, this.inputAction, this.obsecureText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(hint),
        TextField(
          obscureText:obsecureText,
          textInputAction: inputAction,
          onChanged: onChange,
          decoration: InputDecoration(
              hintText: hint,
              hintStyle: TextStyle(
                  color: Colors.grey.shade400,
                  fontStyle: FontStyle.italic,
                  fontSize: 12.0)),
        ),
      ],
    );
  }
}
