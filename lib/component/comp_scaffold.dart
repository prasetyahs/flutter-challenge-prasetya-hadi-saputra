import 'package:flutter/material.dart';

class CompScaffold extends StatelessWidget{
  final Widget body;
  const CompScaffold({Key? key,required this.body}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: body,
    );
  }
}